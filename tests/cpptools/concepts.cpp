#include <gtest/gtest.h>

#include <cpptools/concepts.hpp>

#include <type_traits>

#include <memory>

#include <vector>
#include <array>
#include <list>
#include <deque>
#include <forward_list>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <span>
#include <stack>
#include <queue>
#include <initializer_list>
#include <string>

namespace {

using test_t = uint32_t;

TEST( concepts_tests, is_specialization )
{
  EXPECT_TRUE( ( cpptools::is_specialization_v< std::vector< test_t >, std::vector > ));
  EXPECT_TRUE( ( cpptools::is_specialization_v< std::unique_ptr< test_t >, std::unique_ptr > ));

  EXPECT_FALSE(( cpptools::is_specialization_v< std::map< test_t, uint64_t >, std::vector > ));
}

TEST( concepts_tests, is_smart_pointer )
{
  EXPECT_TRUE( cpptools::is_smart_pointer_v< std::unique_ptr< test_t > > );
  EXPECT_TRUE( cpptools::is_smart_pointer_v< std::shared_ptr< test_t > > );
  EXPECT_TRUE( cpptools::is_smart_pointer_v< std::weak_ptr  < test_t > > );

  EXPECT_FALSE( cpptools::is_smart_pointer_v< test_t >  );
  EXPECT_FALSE( cpptools::is_smart_pointer_v< test_t* > );
}

TEST( concepts_tests, is_any_pointer )
{
  EXPECT_TRUE( cpptools::is_any_pointer_v< std::unique_ptr< test_t > > );
  EXPECT_TRUE( cpptools::is_any_pointer_v< std::shared_ptr< test_t > > );
  EXPECT_TRUE( cpptools::is_any_pointer_v< std::weak_ptr  < test_t > > );

  EXPECT_TRUE( cpptools::is_any_pointer_v< test_t* >  );
  EXPECT_TRUE( cpptools::is_any_pointer_v< test_t****** > );
}

TEST( concepts_tests, is_any_pointer_in )
{
  EXPECT_TRUE( ( cpptools::is_any_pointer_in_v< test_t, test_t*, test_t&& > ));
  EXPECT_TRUE( ( cpptools::is_any_pointer_in_v< test_t, std::shared_ptr< test_t >, test_t&& > ));

  EXPECT_FALSE(( cpptools::is_any_pointer_in_v< test_t, test_t&, test_t&& > ));
  EXPECT_FALSE(  cpptools::is_any_pointer_in_v<> );
}

TEST( concepts_tests, streamables )
{
  EXPECT_TRUE( cpptools::is_streamable_v< cpptools::ios_base > );
  EXPECT_TRUE( cpptools::is_input_streamable_v< cpptools::istream > );
  EXPECT_TRUE( cpptools::is_output_streamable_v< cpptools::ostream > );

  EXPECT_FALSE( cpptools::is_streamable_v< test_t > );
  EXPECT_FALSE( cpptools::is_input_streamable_v< test_t > );
  EXPECT_FALSE( cpptools::is_output_streamable_v< test_t > );
}

TEST( concepts_tests, containers )
{
  EXPECT_TRUE(( cpptools::is_container_v< std::array < test_t, 10 > > ));
  EXPECT_TRUE( cpptools::is_container_v< std::vector< test_t > > );
  EXPECT_TRUE( cpptools::is_container_v< std::deque < test_t > > );
  EXPECT_TRUE( cpptools::is_container_v< std::list< test_t > > );
  EXPECT_TRUE( cpptools::is_container_v< std::set< test_t > > );
  EXPECT_TRUE( cpptools::is_container_v< std::unordered_set< test_t > > );
  EXPECT_TRUE( cpptools::is_container_v< std::multiset< test_t > > );
  EXPECT_TRUE( cpptools::is_container_v< std::unordered_multiset< test_t > > );
  EXPECT_TRUE(( cpptools::is_container_v< std::map< test_t, test_t > > ));
  EXPECT_TRUE(( cpptools::is_container_v< std::unordered_map< test_t, test_t > > ));
  EXPECT_TRUE(( cpptools::is_container_v< std::multimap< test_t, test_t > > ));
  EXPECT_TRUE(( cpptools::is_container_v< std::unordered_multimap< test_t, test_t > > ));
  EXPECT_TRUE( cpptools::is_container_v< std::span< test_t > > );
  EXPECT_TRUE( cpptools::is_container_v< std::basic_string< test_t > > );
  EXPECT_TRUE( cpptools::is_container_v< std::initializer_list< test_t > > );

  EXPECT_FALSE( cpptools::is_container_v< std::forward_list< test_t > > );
  EXPECT_FALSE( cpptools::is_container_v< std::stack< test_t > > );
  EXPECT_FALSE( cpptools::is_container_v< std::queue< test_t > > );
  EXPECT_FALSE( cpptools::is_container_v< std::priority_queue< test_t > > );
  EXPECT_FALSE( cpptools::is_container_v< test_t > );
}

TEST( concepts_tests, adaptor_containers )
{
  EXPECT_TRUE( cpptools::is_adaptor_container_v< std::stack< test_t > > );
  EXPECT_TRUE( cpptools::is_adaptor_container_v< std::queue< test_t > > );
  EXPECT_TRUE( cpptools::is_adaptor_container_v< std::priority_queue< test_t > > );

  EXPECT_FALSE(( cpptools::is_adaptor_container_v< std::array < test_t, 10 > > ));
  EXPECT_FALSE( cpptools::is_adaptor_container_v< std::vector< test_t > > );
  EXPECT_FALSE( cpptools::is_adaptor_container_v< std::deque < test_t > > );
  EXPECT_FALSE( cpptools::is_adaptor_container_v< std::list< test_t > > );
  EXPECT_FALSE( cpptools::is_adaptor_container_v< std::set< test_t > > );
  EXPECT_FALSE( cpptools::is_adaptor_container_v< std::unordered_set< test_t > > );
  EXPECT_FALSE( cpptools::is_adaptor_container_v< std::multiset< test_t > > );
  EXPECT_FALSE( cpptools::is_adaptor_container_v< std::unordered_multiset< test_t > > );
  EXPECT_FALSE(( cpptools::is_adaptor_container_v< std::map< test_t, test_t > > ));
  EXPECT_FALSE(( cpptools::is_adaptor_container_v< std::unordered_map< test_t, test_t > > ));
  EXPECT_FALSE(( cpptools::is_adaptor_container_v< std::multimap< test_t, test_t > > ));
  EXPECT_FALSE(( cpptools::is_adaptor_container_v< std::unordered_multimap< test_t, test_t > > ));
  EXPECT_FALSE( cpptools::is_adaptor_container_v< std::span< test_t > > );
  EXPECT_FALSE( cpptools::is_adaptor_container_v< std::forward_list< test_t > > );
  EXPECT_FALSE( cpptools::is_adaptor_container_v< test_t > );
  EXPECT_FALSE( cpptools::is_adaptor_container_v< std::basic_string< test_t > > );
  EXPECT_FALSE( cpptools::is_adaptor_container_v< std::initializer_list< test_t > > );
}

TEST( concepts_tests, contiguous_data_containers )
{
  EXPECT_TRUE(( cpptools::is_contiguous_data_container_v< std::array < test_t, 10 > > ));
  EXPECT_TRUE( cpptools::is_contiguous_data_container_v< std::vector< test_t > > );
  EXPECT_TRUE( cpptools::is_contiguous_data_container_v< std::initializer_list< test_t > > );
  EXPECT_TRUE( cpptools::is_contiguous_data_container_v< std::span< test_t > > );
  EXPECT_TRUE( cpptools::is_contiguous_data_container_v< std::basic_string< test_t > > );

  EXPECT_FALSE( cpptools::is_contiguous_data_container_v< std::stack< test_t > > );
  EXPECT_FALSE( cpptools::is_contiguous_data_container_v< std::queue< test_t > > );
  EXPECT_FALSE( cpptools::is_contiguous_data_container_v< std::priority_queue< test_t > > );
  EXPECT_FALSE( cpptools::is_contiguous_data_container_v< std::deque < test_t > > );
  EXPECT_FALSE( cpptools::is_contiguous_data_container_v< std::list< test_t > > );
  EXPECT_FALSE( cpptools::is_contiguous_data_container_v< std::set< test_t > > );
  EXPECT_FALSE( cpptools::is_contiguous_data_container_v< std::unordered_set< test_t > > );
  EXPECT_FALSE( cpptools::is_contiguous_data_container_v< std::multiset< test_t > > );
  EXPECT_FALSE( cpptools::is_contiguous_data_container_v< std::unordered_multiset< test_t > > );
  EXPECT_FALSE(( cpptools::is_contiguous_data_container_v< std::map< test_t, test_t > > ));
  EXPECT_FALSE(( cpptools::is_contiguous_data_container_v< std::unordered_map< test_t, test_t > > ));
  EXPECT_FALSE(( cpptools::is_contiguous_data_container_v< std::multimap< test_t, test_t > > ));
  EXPECT_FALSE(( cpptools::is_contiguous_data_container_v< std::unordered_multimap< test_t, test_t > > ));
  EXPECT_FALSE( cpptools::is_contiguous_data_container_v< std::forward_list< test_t > > );
  EXPECT_FALSE( cpptools::is_contiguous_data_container_v< test_t > );
}

}
