#include <gtest/gtest.h>

#include <cpptools/math.hpp>
#include <array>

namespace {

std::array< uint8_t, 3 > __check_numbers = { 0xfa, 0x98, 0x32 };

TEST( math_tests, to_hex )
{
  EXPECT_EQ( cpptools::__detail::__math::to_hex( 15 ), 'f' );
  EXPECT_EQ( cpptools::__detail::__math::to_hex( 9 ),  '9' );

  EXPECT_EQ( cpptools::__detail::__math::to_hex( __check_numbers.data(), __check_numbers.size() ), "fa9832" );
}

TEST( math_tests, from_hex )
{
  EXPECT_EQ( cpptools::__detail::__math::from_hex( 'f' ), 15 );
  EXPECT_EQ( cpptools::__detail::__math::from_hex( '9' ),  9 );

  std::array< uint8_t, 3 > __local_numbers = { 0, 0, 0 };
  cpptools::__detail::__math::from_hex( "fa9832", __local_numbers.data(), __local_numbers.size() );
  EXPECT_EQ( __local_numbers, __check_numbers );
}

}
