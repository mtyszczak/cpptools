#include <gtest/gtest.h>

#include <cpptools/exception.hpp>

namespace {

TEST( exception_tests, cpptools_assert )
{
  EXPECT_NO_THROW( CPPTOOLS_ASSERT( true, "Should not throw" ) );
  EXPECT_THROW( CPPTOOLS_ASSERT( false, "Should throw" ), cpptools::exception );
}

TEST( exception_tests, no_throw_message )
{
  cpptools::exception e;
  EXPECT_EQ( e.get_trip().size(), 0 );
  EXPECT_EQ( std::string{ e.what() }, "Could not display exception message. No trip recorded" );
}

TEST( exception_tests, throw_assert )
{
  try
  {
    CPPTOOLS_ASSERT( false, "Should throw" );
  }
  catch( const cpptools::exception& e )
  {
    EXPECT_EQ( std::string{ e.what() }, "false: Should throw" );

    const auto& trip = e.get_trip();

    EXPECT_EQ( trip.size(), 1 );

    EXPECT_EQ( trip[0].line, 24 );
    EXPECT_EQ( trip[0].type, typeid( cpptools::assert_exception ).name() );
    EXPECT_EQ( trip[0].message, "false: Should throw" );
    EXPECT_TRUE( trip[0].file.find( "exception.cpp" ) != std::string::npos );
    EXPECT_EQ( trip[0].function, "TestBody" );
  }
  catch( ... )
  {
    EXPECT_TRUE( false ); // Should never reach this
  }
}

TEST( exception_tests, throw_rethrow )
{
  try
  {
    try
    {
      CPPTOOLS_ASSERT( false, "Should throw" );
    } CPPTOOLS_CATCH_LOG_RETHROW();
  }
  catch( const cpptools::exception& e )
  {
    EXPECT_EQ( std::string{ e.what() }, "rethrown" );

    const auto& trip = e.get_trip();

    EXPECT_EQ( trip.size(), 2 );

    EXPECT_EQ( trip[0].line, 52 );
    EXPECT_EQ( trip[0].type, typeid( cpptools::assert_exception ).name() );
    EXPECT_EQ( trip[0].message, "false: Should throw" );

    EXPECT_EQ( trip[1].line, 53 );
    EXPECT_EQ( trip[1].type, typeid( cpptools::rethrown_exception ).name() );
    EXPECT_EQ( trip[1].message, "rethrown" );
  }
  catch( ... )
  {
    EXPECT_TRUE( false ); // Should never reach this
  }
}

}
