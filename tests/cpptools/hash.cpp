#include <gtest/gtest.h>

#include <cpptools/hash.hpp>
#include <array>

namespace {

std::array< uint8_t, 3 > __check_numbers = { 0xfa, 0x98, 0x32 };

cpptools::hash< uint8_t, 3 > __check_hash{ __check_numbers.data(), 3 };

TEST( hash_tests, size_t_conversion )
{
  EXPECT_EQ( __check_hash.operator size_t(), 890 );
}

TEST( hash_tests, to_hex )
{
  EXPECT_EQ( __check_hash.operator std::string(), "fa9832" );
}

TEST( hash_tests, from_hex )
{
  cpptools::hash< uint8_t, 3 > from_hex{ std::string{"fa9832"} };
  EXPECT_EQ( from_hex[0], __check_numbers[0] );
  EXPECT_EQ( from_hex[1], __check_numbers[1] );
  EXPECT_EQ( from_hex[2], __check_numbers[2] );
}

TEST( hash_tests, single )
{
  cpptools::hash< uint8_t, 1 > single_hash{ 10 };
  EXPECT_EQ( *single_hash, 10 );
}

TEST( hash_tests, dynamic_hash )
{
  std::string data = "Hello, world!";
  cpptools::hash< uint8_t, 0 > dynamic_hash{ reinterpret_cast< const uint8_t* >( data.c_str() ), data.size() };
  EXPECT_EQ( dynamic_hash.operator size_t(), 10802 );
}

TEST( hash_tests, compare )
{
  cpptools::hash< uint8_t, 3 > sec{ __check_numbers.data(), 3 };
  EXPECT_TRUE( sec == __check_hash );
  EXPECT_FALSE( sec != __check_hash );
  EXPECT_FALSE( sec > __check_hash );
  EXPECT_FALSE( sec < __check_hash );
}

}
