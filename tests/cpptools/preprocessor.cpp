#include <gtest/gtest.h>

#include <cpptools/preprocessor.hpp>

namespace {

TEST( preprocessor_tests, stringize )
{
  EXPECT_EQ( CPPTOOLS_STRINGIZE( DATA ), "DATA" );
  EXPECT_EQ( CPPTOOLS_STRINGIZE( 0123456789 ), "0123456789" );
  EXPECT_EQ( CPPTOOLS_STRINGIZE( !@#$%^&* ), "!@#$%^&*" );
}

TEST( preprocessor_tests, multiline_macro )
{
  CPPTOOLS_MULTILINE_MACRO_BEGIN(); // Should compile without errors
  CPPTOOLS_MULTILINE_MACRO_END(); // Should compile without errors
}

}
