#include <gtest/gtest.h>

#include <cpptools/vector.hpp>

#include <type_traits>

namespace {

using vector1_underlying_t = uint32_t;
using vector2_underlying_t = int;

using vector1_t = cpptools::vector< vector1_underlying_t, 1 >;
using vector2_t = cpptools::vector< vector2_underlying_t, 3 >;

vector1_t vector1;
vector2_t vector2;

TEST( vector_tests, setting_values )
{
  EXPECT_EQ( vector1.emplace_back( 1 ), 1 );

  vector2.emplace_back();
  vector2[0].emplace_back();
  EXPECT_EQ( vector2[0][0].emplace_back( 1 ), 1 );
}

TEST( vector_tests, size )
{
  EXPECT_EQ( vector1.size(), 1 );

  EXPECT_EQ( vector2.size(),       1 );
  EXPECT_EQ( vector2[0].size(),    1 );
  EXPECT_EQ( vector2[0][0].size(), 1 );
}

TEST( vector_tests, compare )
{
  vector2_t sec;
  sec.emplace_back();
  sec[0].emplace_back();
  sec[0][0].emplace_back( 1 );
  EXPECT_TRUE( sec == vector2 );
  EXPECT_FALSE( sec != vector2 );
  EXPECT_FALSE( sec > vector2 );
  EXPECT_FALSE( sec < vector2 );
}

}
