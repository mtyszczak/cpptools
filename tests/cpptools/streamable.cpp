#include <gtest/gtest.h>

#include <cpptools/streamable.hpp>
#include <cpptools/array.hpp>
#include <cpptools/vector.hpp>
#include <cpptools/hash.hpp>

#include <functional>

#include <type_traits>

#include <memory>

#include <variant>

#include <optional>

#include <vector>
#include <valarray>
#include <array>
#include <list>
#include <deque>
#include <forward_list>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <span>
#include <stack>
#include <queue>
#include <initializer_list>
#include <string>
#include <valarray>
#include <complex>
#include <atomic>

#include <chrono>

namespace {

cpptools::raw::iostream ios;

TEST( streamable_tests, write_counter )
{
  cpptools::null::write_counter wc;

  wc.write( nullptr, 10 );
  EXPECT_EQ( wc.get_total(), 10 );

  wc.write( nullptr, 7 );
  EXPECT_EQ( wc.get_total(), 17 );
}

TEST( streamable_tests, read_counter )
{
  cpptools::null::read_counter rc;

  rc.readsome( nullptr, 7 );
  EXPECT_EQ( rc.get_total(), 7 );

  rc.readsome( nullptr, 10 );
  EXPECT_EQ( rc.get_total(), 17 );
}

TEST( streamable_tests, read_write_counter )
{
  cpptools::null::read_write_counter rwc;

  rwc.readsome( nullptr, 7 );
  EXPECT_EQ( rwc.get_total(), 7 );

  rwc.write( nullptr, 10 );
  EXPECT_EQ( rwc.get_total(), 17 );
}

TEST( streamable_tests, SHA256 )
{
  cpptools::SHA256::encoder e;

  static constexpr uint8_t hash[] = {
    0x31, 0x5f, 0x5b, 0xdb, 0x76, 0xd0, 0x78, 0xc4,
    0x3b, 0x8a, 0xc0, 0x06, 0x4e, 0x4a, 0x01, 0x64,
    0x61, 0x2b, 0x1f, 0xce, 0x77, 0xc8, 0x69, 0x34,
    0x5b, 0xfc, 0x94, 0xc7, 0x58, 0x94, 0xed, 0xd3
  };

  e.write( reinterpret_cast< const uint8_t* >( "Hello, world!" ), 13 );

  EXPECT_EQ( 0, memcmp( &hash[0], e.digest(), 32 ) );
}

TEST( streamable_tests, SHA256_stringified )
{
  cpptools::SHA256::encoder e;

  e.write( reinterpret_cast< const uint8_t* >( "Hello, world!" ), 13 );

  EXPECT_EQ( "315f5bdb76d078c43b8ac0064e4a0164612b1fce77c869345bfc94c75894edd3", e.to_string() );
}

template< typename T >
using struct_equality_function_t = std::function< void( std::decay_t<T>&, std::decay_t<T>& ) >;

template< typename T >
const auto default_struct_equality_function = []( const std::decay_t<T>& input, const std::decay_t<T>& output )
{
  EXPECT_EQ( input, output );
};

template< typename T >
void check_values( T input, struct_equality_function_t<T> equal_check_function = default_struct_equality_function<T> )
{
  T output;

  pack( ios, input );
  unpack( ios, output );

  EXPECT_EQ( ios.size(), 0 );
  equal_check_function( input, output );
}

TEST( streamable_tests, primitives )
{
  check_values< char >( 1 );
  check_values< unsigned char >( 2 );
  check_values< bool >( true );

  check_values<  uint8_t >( 10 );
  check_values<   int8_t >( 11 );
  check_values< uint16_t >( 12 );
  check_values<  int16_t >( 13 );
  check_values< uint32_t >( 14 );
  check_values<  int32_t >( 15 );
  check_values< uint64_t >( 16 );
  check_values<  int64_t >( 17 );

  check_values< float  >( 3.1415 );
  check_values< double  >( 3.141592 );
}

TEST( streamable_tests, contiguous_containers )
{
  check_values< std::array< uint32_t, 3 > >( { 1, 2, 3 } );
  check_values< std::vector< uint32_t > >( { 1, 2, 3 } );
  // check_values< std::initializer_list< uint32_t > >( { 1, 2, 3 } ); // You cannot initialize initializer_list at runtime
  //std::vector< uint32_t > data = { 1, 2, 3 };
  /*check_values< std::span< uint32_t > >( std::span<uint32_t>{ data.begin(), 3 }, []( const auto& input, const auto& output ) {
    std::cout << output.data() << std::endl;
    EXPECT_EQ( input.size(), output.size() );
    for( size_t i = 0; i < input.size(); ++i )
      EXPECT_EQ( input[i], output[i] );
  } );*/ // You cannot initialize span with external stack allocated vector (see streamable.hpp)
  check_values< std::basic_string< uint32_t > >( { 1, 2, 3 } );
}

TEST( streamable_tests, adaptor_containers )
{
  std::stack< uint32_t > ac1;
  ac1.push( 1 ); ac1.push( 2 ); ac1.push( 3 );
  check_values< std::stack< uint32_t > >( ac1 );

  std::queue< uint32_t > ac2;
  ac2.push( 1 ); ac2.push( 2 ); ac2.push( 3 );
  check_values< std::queue< uint32_t > >( ac2 );

  std::priority_queue< uint32_t > ac3;
  ac3.push( 1 ); ac3.push( 2 ); ac3.push( 3 );
  check_values< std::priority_queue< uint32_t > >( ac3, []( auto& input, auto& output ) {
    EXPECT_EQ( input.size(), output.size() );
    for( size_t i = 0; i < input.size(); ++i )
    {
      EXPECT_EQ( input.top(), output.top() );
      input.pop();
      output.pop();
    }
  } );
}

TEST( streamable_tests, pair )
{
  check_values< std::pair< uint8_t, uint16_t > >( { 1, 2 } );
}

TEST( streamable_tests, containers )
{
  check_values< std::forward_list< uint32_t > >( { 1, 2, 3 } );
  check_values< std::deque< uint32_t > >( { 1, 2, 3 } );
  check_values< std::list< uint32_t > >( { 1, 2, 3 } );
  check_values< std::set< uint32_t > >( { 1, 2, 3 } );
  check_values< std::unordered_set< uint32_t > >( { 1, 2, 3 } );
  check_values< std::multiset< uint32_t > >( { 1, 2, 3 } );
  check_values< std::unordered_multiset< uint32_t > >( { 1, 2, 3 } );
  check_values< std::map< uint32_t, uint32_t > >( { { 1, 1 }, { 2, 2 }, { 3, 3 } } );
  check_values< std::unordered_map< uint32_t, uint32_t > >( { { 1, 1 }, { 2, 2 }, { 3, 3 } } );
  check_values< std::multimap< uint32_t, uint32_t > >( { { 1, 1 }, { 2, 2 }, { 3, 3 } } );
  check_values< std::unordered_multimap< uint32_t, uint32_t > >( { { 1, 1 }, { 2, 2 }, { 3, 3 } } );
}

TEST( streamable_tests, variant )
{
  check_values< std::variant< char, uint16_t, std::string > >( { std::string{"Hello, world!"} } );
}

TEST( streamable_tests, optional )
{
  check_values< std::optional< uint16_t > >( { 2 } );
}

TEST( streamable_tests, tuple )
{
  check_values< std::tuple< char, size_t, std::string > >( { '!', 1, "Hello, world!" } );
}

TEST( streamable_tests, duration )
{
  std::chrono::duration< uint32_t > dur{ 1000 };
  check_values< decltype(dur) >( dur );
}

TEST( streamable_tests, time_point )
{
  check_values< std::chrono::time_point< std::chrono::system_clock > >( std::chrono::time_point<std::chrono::system_clock>::max() );
}

TEST( streamable_tests, complex )
{
  check_values< std::complex< uint32_t > >( { 1, 2 } );
}

TEST( streamable_tests, atomic )
{
  check_values< std::atomic< uint32_t > >( { 12345678 } );
}

TEST( streamable_tests, atomic_ref )
{
  uint32_t data = 12345678, fake_data = -1;
  std::atomic_ref< uint32_t > ref1{ data };
  std::atomic_ref< uint32_t > ref2{ fake_data };

  pack( ios, ref1 );
  unpack( ios, ref2 );

  EXPECT_EQ( ios.size(), 0 );
  EXPECT_EQ( ref1, ref2 );
}

TEST( streamable_tests, reference_wrapper )
{
  uint32_t data = 12345678, fake_data = -1;
  std::reference_wrapper< uint32_t > ref1{ data };
  std::reference_wrapper< uint32_t > ref2{ fake_data };

  pack( ios, ref1 );
  unpack( ios, ref2 );

  EXPECT_EQ( ios.size(), 0 );
  EXPECT_EQ( ref1, ref2 );
}

TEST( streamable_tests, valarray )
{
  std::vector< uint32_t > data = { 1, 2, 3 };
  check_values< std::valarray< uint32_t > >( { data.data(), 3 }, []( const auto& input, const auto& output ) {
    EXPECT_EQ( input.size(), output.size() );
    for( size_t i = 0; i < input.size(); ++i )
      EXPECT_EQ( input[i], output[i] );
  } );
}

TEST( streamable_tests, array )
{
  cpptools::array< uint8_t, 2, 2 > ar = { { {1, 2}, {3, 4} } };

  check_values< cpptools::array< uint8_t, 2, 2 > >( ar, []( const auto& input, const auto& output ) {
    for( size_t i = 0; i < input.size(); ++i )
      for( size_t j = 0; j < input[0].size(); ++j )
        EXPECT_EQ( input[i][j], output[i][j] );
  } );
}

TEST( streamable_tests, hash )
{
  cpptools::hash< uint8_t, 2 > h{ 1, 2 };

  check_values< cpptools::hash< uint8_t, 2 > >( h, []( const auto& input, const auto& output ) {
    for( size_t i = 0; i < input.size(); ++i )
        EXPECT_EQ( input[i], output[i] );
  } );
}

TEST( streamable_tests, vector )
{
  cpptools::vector< uint8_t, 2 > h;
  h.emplace_back();
  h.emplace_back();
  h[0].emplace_back( 1 );
  h[0].emplace_back( 2 );
  h[1].emplace_back( 3 );
  h[1].emplace_back( 4 );

  check_values< cpptools::vector< uint8_t, 2 > >( h, []( const auto& input, const auto& output ) {
    EXPECT_EQ( input.size(), output.size() );
    for( size_t i = 0; i < input.size(); ++i )
      for( size_t j = 0; j < input[0].size(); ++j )
        EXPECT_EQ( input[i][j], output[i][j] );
  } );
}

}
