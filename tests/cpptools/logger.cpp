#include <gtest/gtest.h>

#include <cpptools/logger.hpp>

namespace {

TEST( logger_tests, dlog )
{
  cpptools_dlog << "Hello," << cpptools::fnl << "debug!" << cpptools::endl;
}

TEST( logger_tests, ilog )
{
  cpptools_ilog << "Hello," << cpptools::fnl << "information!" << cpptools::endl;
}

TEST( logger_tests, wlog )
{
  cpptools_wlog << "Hello," << cpptools::fnl << "warning!" << cpptools::endl;
}

TEST( logger_tests, elog )
{
  cpptools_elog << "Hello," << cpptools::fnl << "error!" << cpptools::endl;
}

}
