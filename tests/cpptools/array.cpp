#include <gtest/gtest.h>

#include <cpptools/array.hpp>

#include <type_traits>

namespace {

using array1_underlying_t = uint32_t;
using array2_underlying_t = int;

using array1_t = cpptools::array< array1_underlying_t, 30 >;
using array2_t = cpptools::array< array2_underlying_t, 3, 5, 2 >;

array1_t array1;
array2_t array2;

TEST( array_tests, underlying_type )
{
  EXPECT_TRUE(( std::is_same< array1_underlying_t, array1_t::underlying_type >::value ));
  EXPECT_TRUE(( std::is_same< array2_underlying_t, array2_t::underlying_type >::value ));
}

TEST( array_tests, total_size )
{
  EXPECT_EQ( array1_t::total_size, 30 );
  EXPECT_EQ( array2_t::total_size, 30 );
}

TEST( array_tests, setting_values )
{
  EXPECT_EQ( array1[0] = 1, 1 );

  EXPECT_EQ( array2[0][0][0] = 1, 1 );
}

TEST( array_tests, size )
{
  EXPECT_EQ( array1.size(), 30 );

  EXPECT_EQ( array2.size(),       3 );
  EXPECT_EQ( array2[0].size(),    5 );
  EXPECT_EQ( array2[0][0].size(), 2 );
}

TEST( array_tests, sizeof_class )
{
  EXPECT_EQ( sizeof( array1 ), array1_t::total_size * sizeof( array1_t::underlying_type ) );
  EXPECT_EQ( sizeof( array2 ), array2_t::total_size * sizeof( array2_t::underlying_type ) );
}

TEST( array_tests, compare )
{
  array2_t sec = {{{1}}};
  EXPECT_TRUE( sec == array2 );
  EXPECT_FALSE( sec != array2 );
  EXPECT_FALSE( sec > array2 );
  EXPECT_FALSE( sec < array2 );
}

}
