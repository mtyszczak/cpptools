# cpptools
C++ tools library

## Content
| name | include | description | since |
|------|---------|-------------|-------|
| concepts | `cpptools/concepts.hpp` | Concepts and type traits extension structs | `0.1` |
| exception | `cpptools/exception.hpp` | Exception handling rethrowing, logging | `0.1` |
| hash | `cpptools/hash.hpp` | Universal hash wrapper based on the cpptools multidimensional array | `0.1` |
| logger | `cpptools/logger.hpp` | logger utility that supports multiple logger names, filename, line number, function names | `0.1` |
| math | `cpptools/math.hpp` | math utilities like number format conversion shared with cpptools | `0.1` |
| array | `cpptools/array.hpp` | One or multi dimensional array implementation | `0.1` |
| preprocessor | `cpptools/preprocessor.hpp` | Basic preprocessor utilities shared with cpptools | `0.1` |
| streamable | `cpptools/streamable.hpp` | Streamable library for serialization and hashing | `0.1` |
| tuple | `cpptools/tuple.hpp` | Tuple utilities shared with cpptools | `0.1` |
| variant | `cpptools/variant.hpp` | Variant utilities shared with cpptools | `0.1` |
| vector | `cpptools/vector.hpp` | One or multi dimensional vector implementation | `0.1` |

## Building
### Compile-Time Options (cmake)
#### CPPTOOLS_STATIC_BUILD=[ON/OFF]
Builds cpptools as a static library instead of shared library

### Building on Ubuntu 18.04/20.04

```sh
sudo apt-get update

# Required packages
sudo apt-get install -y \
    cmake \
    g++ \
    git \
    make

git clone https://gitlab.com/mtyszczak/cpptools.git
cd cpptools
git checkout master
git submodule update --init --recursive --progress
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j$(nproc) cpptools

# optional
make install # defaults to /usr/local

```

## License
Distributed under the GNU GENERAL PUBLIC LICENSE Version 3 See [LICENSE.md](LICENSE.md)
