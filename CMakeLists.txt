cmake_minimum_required( VERSION 3.20 )
project( "cpptools" VERSION 0.1 DESCRIPTION "C++ tools" LANGUAGES CXX )

# C++ setup
set( CMAKE_CXX_STANDARD 23 )
set( CMAKE_CXX_STANDARD_REQUIRED OFF )
set( CMAKE_CXX_EXTENSIONS OFF )
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Werror -Wpedantic")
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Release" )
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3")
endif()

option( CPPTOOLS_STATIC_BUILD "Build cpptools as a static library (ON/OFF)" OFF )

option( CPPTOOLS_BUILD_TESTS "Build cpptools tests (ON/OFF)" OFF )

file( GLOB_RECURSE CPPTOOLS_SOURCES src/*.cpp )

file( GLOB CPPTOOLS_INCLUDES include )

add_subdirectory( vendor )

if( CPPTOOLS_STATIC_BUILD )
  add_library( cpptools STATIC ${CPPTOOLS_SOURCES} )
else()
  add_library( cpptools SHARED ${CPPTOOLS_SOURCES} )
endif()

set_target_properties( cpptools PROPERTIES VERSION ${PROJECT_VERSION} SOVERSION ${PROJECT_VERSION_MAJOR} )

target_include_directories( cpptools PUBLIC ${CPPTOOLS_INCLUDES} )

target_link_libraries( cpptools SHA256 )

include( GNUInstallDirs )

install( TARGETS cpptools
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR} )

if( CPPTOOLS_BUILD_TESTS )
  add_subdirectory( tests )
endif()
