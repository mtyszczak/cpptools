#pragma once

#include <cstring>
#include <variant>
#include <cassert>

namespace cpptools {

namespace __detail { namespace __tuple {

  template< typename T, typename V, size_t... I >
  void visit_impl( T&& tuple, V&& visitor, std::index_sequence< I... > )
  {
    (..., visitor( std::get<I>( tuple ) ));
  }

  template< typename T, typename V >
  void visit( T&& tuple, V&& visitor )
  {
    visit_impl( std::forward<T>( tuple ), std::forward<V>( visitor ),
      std::make_index_sequence< std::tuple_size< typename std::decay<T>::type>::value>() );
  }

} } // __detail::__tuple

} // cpptools
