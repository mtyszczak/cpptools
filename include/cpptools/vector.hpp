#pragma once

#include <vector>
#include <initializer_list>

namespace cpptools {

  template< typename T, size_t Dim = 1 >
  class vector : public std::vector< typename vector<T,Dim-1>::storage_type >
  {
  public:
    using parent = vector<T,Dim-1>;
    using storage_type = std::vector< typename parent::storage_type >;

    constexpr vector() = default;

    constexpr vector( const T* other, size_t size )
      : std::vector< typename vector<T,Dim-1>::storage_type >( other, other+size )
    {}

    constexpr vector( const T* first, const T* last )
      : std::vector< typename vector<T,Dim-1>::storage_type >( first, last )
    {}

    constexpr vector( const std::initializer_list< T >& l )
      : std::vector< typename vector<T,Dim-1>::storage_type >( l )
    {}

    // TODO: Implement aggregate initialization of multiple vector dimens
  };

  template< typename T >
  class vector<T,0>
  {};

  template< typename T >
  class vector<T,1> : public std::vector<T>
  {
  public:
    using storage_type = std::vector<T>;

    constexpr vector() = default;

    constexpr vector( const T* other, size_t size )
      : std::vector<T>( other, other+size )
    {}

    constexpr vector( const T* first, const T* last )
      : std::vector<T>( first, last )
    {}

    constexpr vector( const std::initializer_list< T >& l )
      : std::vector<T>( l )
    {}
  };

} // cpptools
