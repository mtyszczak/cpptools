#pragma once

#define CPPTOOLS_STRINGIZE_I( data ) #data
#define CPPTOOLS_STRINGIZE( data ) CPPTOOLS_STRINGIZE_I( data )

#define CPPTOOLS_MULTILINE_MACRO_BEGIN() \
          do {

#define CPPTOOLS_MULTILINE_MACRO_END()   \
          } while( false )
