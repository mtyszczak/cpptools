#pragma once

#include <cpptools/logger.hpp>
#include <cpptools/preprocessor.hpp>

#include <exception>
#include <string>
#include <typeinfo>
#include <utility>

namespace cpptools {

  static inline const standard_logger error_log{ "error_log" };

  struct unknown_exception {};
  struct rethrown_exception {};
  struct assert_exception {};

  struct exception_trip_node
  {
    unsigned    line;
    std::string type;
    std::string message;
    std::string file;
    std::string function;
  };

  class exception
  {
  public:
    using trip_type = std::vector< exception_trip_node >;

    exception() = default;

    exception( const exception_trip_node& _next );
    exception( const trip_type& __rhs );
    exception( const trip_type& __rhs, const exception_trip_node& _next );

    exception(const exception&) = default;
    exception& operator=(const exception&) = default;
    exception(exception&&) = default;
    exception& operator=(exception&&) = default;
    virtual ~exception() noexcept = default;

    virtual void record( const exception_trip_node& node );

    const trip_type& get_trip()const;

    void display_trip()const;

    virtual const char* what()const;

  private:
    trip_type trip;
  };

#define CPPTOOLS_THROW_EXCEPTION( e )   \
  CPPTOOLS_MULTILINE_MACRO_BEGIN()      \
  throw cpptools::exception{ e };       \
  CPPTOOLS_MULTILINE_MACRO_END()

#define CPPTOOLS_RETHROW_EXCEPTION( e ) \
  CPPTOOLS_MULTILINE_MACRO_BEGIN()      \
  throw cpptools::exception{ e.get_trip(), (cpptools::exception_trip_node{ __LINE__, typeid( cpptools::rethrown_exception{} ).name(), "rethrown", __FILE__, __func__ }) };  \
  CPPTOOLS_MULTILINE_MACRO_END()

#define CPPTOOLS_ASSERT( expr, ... )    \
  CPPTOOLS_MULTILINE_MACRO_BEGIN()      \
  if( !( expr ) )                       \
    CPPTOOLS_THROW_EXCEPTION( ( cpptools::exception_trip_node{ __LINE__, typeid( cpptools::assert_exception{} ).name(), #expr ": " __VA_ARGS__, __FILE__, __func__ } ) ); /* TODO: msg variadic to logger */ \
  CPPTOOLS_MULTILINE_MACRO_END()

#define CPPTOOLS_CATCH_LOG_CALL_RETHROW( call ) \
  catch( cpptools::exception& e )          \
  {                                        \
    call();                                \
    e.display_trip();                      \
    CPPTOOLS_RETHROW_EXCEPTION( e );       \
  }                                        \
  catch( const std::exception& e )         \
  {                                        \
    call();                                \
    CPPTOOLS_ELOG_WITH_LOGGER( (::cpptools::error_log), (e.what()) );                \
    CPPTOOLS_RETHROW_EXCEPTION( (cpptools::exception{ cpptools::exception_trip_node{ __LINE__, typeid( e ).name(), e.what(), __FILE__, __func__ } }) ); \
  }                                        \
  catch( ... )                             \
  {                                        \
    call();                                \
    CPPTOOLS_ELOG_WITH_LOGGER( (::cpptools::error_log), ("unknown exception") );     \
    CPPTOOLS_RETHROW_EXCEPTION( (cpptools::exception{ cpptools::exception_trip_node{ __LINE__, typeid( cpptools::unknown_exception{} ).name(), "unknown exception", __FILE__, __func__ } }) ); \
  }

#define CPPTOOLS_CATCH_CALL_RETHROW( call )\
  catch( cpptools::exception& e )          \
  {                                        \
    call();                                \
    CPPTOOLS_RETHROW_EXCEPTION( e );       \
  }                                        \
  catch( const std::exception& e )         \
  {                                        \
    call();                                \
    CPPTOOLS_RETHROW_EXCEPTION( (cpptools::exception{ cpptools::exception_trip_node{ __LINE__, typeid( e ).name(), e.what(), __FILE__, __func__ } }) ); \
  }                                        \
  catch( ... )                             \
  {                                        \
    call();                                \
    CPPTOOLS_RETHROW_EXCEPTION( (cpptools::exception{ cpptools::exception_trip_node{ __LINE__, typeid( cpptools::unknown_exception{} ).name(), "unknown exception", __FILE__, __func__ } }) ); \
  }

#define CPPTOOLS_CATCH_LOG_CALL( call )    \
  catch( const cpptools::exception& e )    \
  {                                        \
    call();                                \
    e.display_trip();                      \
  }                                        \
  catch( const std::exception& e )         \
  {                                        \
    call();                                \
    CPPTOOLS_ELOG_WITH_LOGGER( (::cpptools::error_log), (e.what()) );            \
  }                                        \
  catch( ... )                             \
  {                                        \
    call();                                \
    CPPTOOLS_ELOG_WITH_LOGGER( (::cpptools::error_log), ("unknown exception") ); \
  }

#define CPPTOOLS_CATCH_CALL( call )        \
  catch( const cpptools::exception& e )    \
  {                                        \
    call();                                \
  }                                        \
  catch( const std::exception& e )         \
  {                                        \
    call();                                \
  }                                        \
  catch( ... )                             \
  {                                        \
    call();                                \
  }

#define CPPTOOLS_CATCH_LOG_RETHROW() CPPTOOLS_CATCH_LOG_CALL_RETHROW( [](){} )
#define CPPTOOLS_CATCH_LOG() CPPTOOLS_CATCH_LOG_CALL( [](){} )

#define CPPTOOLS_CATCH_RETHROW() CPPTOOLS_CATCH_CALL_RETHROW( [](){} )
#define CPPTOOLS_CATCH() CPPTOOLS_CATCH_CALL( [](){} )

} // cpptools
