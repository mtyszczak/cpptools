#pragma once

#include <cassert>

#include <cstring>

#include <string>
#include <vector>
#include <variant>
#include <utility>
#include <span>
#include <tuple>
#include <list>
#include <optional>

#include <queue>
#include <stack>
#include <deque>
#include <forward_list>

#include <valarray>
#include <complex>
#include <atomic>

#include <chrono>

#include <type_traits>

#include <SHA256.h>

#include <cpptools/concepts.hpp>
#include <cpptools/variant.hpp>
#include <cpptools/tuple.hpp>
#include <cpptools/vector.hpp>

namespace cpptools {

class ios_base
{
public:
  typedef std::vector< uint8_t > raw_data_type;

  virtual void write( const uint8_t* data, size_t length ) = 0;
  virtual void readsome(    uint8_t* data, size_t length ) = 0;

  const raw_data_type& get_data()const;

  size_t size()const;

protected:
  raw_data_type data;
};

class istream : virtual public ios_base
{
public:
  virtual void write( const uint8_t* data, size_t length ) override;
  virtual void readsome(    uint8_t* data, size_t length ) override = 0;
};

class ostream : virtual public ios_base
{
public:
  virtual void write( const uint8_t* data, size_t length ) override = 0;
  virtual void readsome(    uint8_t* data, size_t length ) override;
};

/**
 * @brief everything in this namespace does not perform any writes/reads
 * @see null::counter
 */
namespace null {
  /**
   * @brief For later identification (if performs any writes/reads)
   */
  class counter {
  public:
    size_t get_total()const;

  protected:
    size_t total = 0;
  };

  /**
   * @brief counts length from all of the writes
   */
  class read_counter : virtual public counter, virtual public istream
  {
  public:
    virtual void readsome( uint8_t* data, size_t length ) override;
  };

  /**
   * @brief counts length from all of the writes
   */
  class write_counter : virtual public counter, virtual public ostream
  {
  public:
    virtual void write( const uint8_t* data, size_t length ) override;
  };

  class read_write_counter final : public read_counter, public write_counter
  {
  public:
    virtual void write( const uint8_t* data, size_t length ) override;
    virtual void readsome( uint8_t* data, size_t length ) override;
  };
}

namespace SHA256 {
  class encoder final : public SHA256, public ostream
  {
  public:
    virtual void write( const uint8_t* data, size_t length ) override;

    uint8_t* digest() /* override */;

    uint8_t data[ 32 ];
  };
} // SHA256

namespace raw {
  class ostream : virtual public ::cpptools::ostream
  {
  public:
    virtual void write( const uint8_t* data, size_t length ) override;
  };

  class istream : virtual public ::cpptools::istream
  {
  public:
    virtual void readsome( uint8_t* data, size_t length ) override;

  private:
    size_t current_offset = 0;
  };

  class iostream final : public istream, public ostream
  {
  public:
    virtual void write( const uint8_t* data, size_t length ) override;
    virtual void readsome( uint8_t* data, size_t length ) override;
  };

  template< typename T >
  inline ostream::raw_data_type pack_to_vector( const T& data )
  {
    ostream ostr;
    pack( ostr, data );
    return ostr.get_data();
  }

  template< typename T >
  inline T& unpack_from_vector( const iostream::raw_data_type& data )
  {
    iostream iostr;
    iostr.write( data.data(), data.size() ); // Pass the vector to the iostream
    T t; // Create local variable
    unpack( iostr, t ); // unpack vector to it
    return t;
  }
} // raw

namespace __detail { namespace __streamable {

  template< ContiguousDataContainer T >
  struct before_contiguous_unpack
  { void operator()( const T&, size_t )const{} };

  template< typename T, typename Allocator >
  struct before_contiguous_unpack< std::vector< T, Allocator > >
  {
    void operator()( std::vector< T, Allocator >& data, size_t size )
      { data.resize( size ); }
  };

  template< typename T, size_t N >
  struct before_contiguous_unpack< cpptools::vector< T, N > >
  {
    void operator()( cpptools::vector< T, N >& data, size_t size )
      { data.resize( size ); }
  };

  template< typename CharT, typename Traits, typename Allocator >
  struct before_contiguous_unpack< std::basic_string< CharT, Traits, Allocator > >
  {
    void operator()( std::basic_string< CharT, Traits, Allocator >& data, size_t size )
      { data.resize( size ); }
  };

  template< typename T >
  concept MapType = ( is_specialization_v< T, std::map >      || is_specialization_v< T, std::unordered_map >
                  || is_specialization_v< T, std::multimap > || is_specialization_v< T, std::unordered_multimap > )
                  && Container< T >;

} } // __detail::__streamable

template< OutputStreamable StreamType > inline void pack( StreamType& s, char data )
  { s.write( reinterpret_cast< const uint8_t* >( &data ), sizeof(char) ); }
template< OutputStreamable StreamType > inline void pack( StreamType& s, bool data )
  { s.write( reinterpret_cast< const uint8_t* >( &data ), sizeof(bool) ); }

template< InputStreamable StreamType > inline void unpack( StreamType& s, char& data )
  { s.readsome( reinterpret_cast< uint8_t* >( &data ), sizeof(char) ); }
template< InputStreamable StreamType > inline void unpack( StreamType& s, bool& data )
  { s.readsome( reinterpret_cast< uint8_t* >( &data ), sizeof(bool) ); }

template< OutputStreamable StreamType > inline void pack( StreamType& s, uint8_t data )
  { s.write( static_cast< const uint8_t* >( &data ), sizeof(uint8_t) ); }
template< OutputStreamable StreamType > inline void pack( StreamType& s, int8_t data )
  { s.write( reinterpret_cast< const uint8_t* >( &data ), sizeof(int8_t) ); }

template< InputStreamable StreamType > inline void unpack( StreamType& s, uint8_t& data )
  { s.readsome( &data, sizeof(uint8_t) ); }
template< InputStreamable StreamType > inline void unpack( StreamType& s, int8_t& data )
  { s.readsome( reinterpret_cast< uint8_t* >( &data ), sizeof(int8_t) ); }

template< OutputStreamable StreamType > inline void pack( StreamType& s, uint16_t data )
  { s.write( reinterpret_cast< const uint8_t* >( &data ), sizeof(uint16_t) ); }
template< OutputStreamable StreamType > inline void pack( StreamType& s, int16_t data )
  { s.write( reinterpret_cast< const uint8_t* >( &data ), sizeof(int16_t) ); }

template< InputStreamable StreamType > inline void unpack( StreamType& s, uint16_t& data )
  { s.readsome( reinterpret_cast< uint8_t* >( &data ), sizeof(uint16_t) ); }
template< InputStreamable StreamType > inline void unpack( StreamType& s, int16_t& data )
  { s.readsome( reinterpret_cast< uint8_t* >( &data ), sizeof(int16_t) ); }

template< OutputStreamable StreamType > inline void pack( StreamType& s, uint32_t data )
  { s.write( reinterpret_cast< const uint8_t* >( &data ), sizeof(uint32_t) ); }
template< OutputStreamable StreamType > inline void pack( StreamType& s, int32_t data )
  { s.write( reinterpret_cast< const uint8_t* >( &data ), sizeof(int32_t) ); }

template< InputStreamable StreamType > inline void unpack( StreamType& s, uint32_t& data )
  { s.readsome( reinterpret_cast< uint8_t* >( &data ), sizeof(uint32_t) ); }
template< InputStreamable StreamType > inline void unpack( StreamType& s, int32_t& data )
  { s.readsome( reinterpret_cast< uint8_t* >( &data ), sizeof(int32_t) ); }

template< OutputStreamable StreamType > inline void pack( StreamType& s, uint64_t data )
  { s.write( reinterpret_cast< const uint8_t* >( &data ), sizeof(uint64_t) ); }
template< OutputStreamable StreamType > inline void pack( StreamType& s, int64_t data )
  { s.write( reinterpret_cast< const uint8_t* >( &data ), sizeof(int64_t) ); }

template< InputStreamable StreamType > inline void unpack( StreamType& s, uint64_t& data )
  { s.readsome( reinterpret_cast< uint8_t* >( &data ), sizeof(uint64_t) ); }
template< InputStreamable StreamType > inline void unpack( StreamType& s, int64_t& data )
  { s.readsome( reinterpret_cast< uint8_t* >( &data ), sizeof(int64_t) ); }

template< OutputStreamable StreamType > inline void pack( StreamType& s, float data )
  { s.write( reinterpret_cast< const uint8_t* >( &data ), sizeof(float) ); }
template< OutputStreamable StreamType > inline void pack( StreamType& s, double data )
  { s.write( reinterpret_cast< const uint8_t* >( &data ), sizeof(double) ); }

template< InputStreamable StreamType > inline void unpack( StreamType& s, float& data )
  { s.readsome( reinterpret_cast< uint8_t* >( &data ), sizeof(float) ); }
template< InputStreamable StreamType > inline void unpack( StreamType& s, double& data )
  { s.readsome( reinterpret_cast< uint8_t* >( &data ), sizeof(double) ); }

template< OutputStreamable StreamType, typename T, size_t N >
inline void pack( StreamType& s, const T(&data)[N] ) // array with specified bounds
  requires ( !is_any_pointer_v< T > )
  {
    pack( s, N );
    if constexpr( std::is_fundamental_v<T> )
      s.write( reinterpret_cast< const uint8_t* >( data ), N * sizeof( T ) );
    else
      for( const auto& d : data )
        pack( s, d );
  }
template< InputStreamable StreamType, typename T, size_t N >
inline void unpack( StreamType& s, T(&data)[N] ) // array with specified bounds
  requires ( !is_any_pointer_v< T > )
  {
    size_t size = 0;
    unpack( s, size );
    assert( N == size && "Array size mismatch" );
    if constexpr( std::is_fundamental_v<T> )
      s.readsome( reinterpret_cast< uint8_t* >( data ), N * sizeof( T ) );
    else
      for( auto& d : data )
        unpack( s, d );
  }

template< OutputStreamable StreamType, typename T, typename Allocator = std::allocator<T> >
inline void pack( StreamType& s, std::forward_list< T, Allocator >& data )
  {
    size_t size = 0;
    for( auto it = data.begin(); it != data.end(); ++it ) ++size;
    pack( s, static_cast< size_t >( size ) );
    auto local = data;
    local.reverse();
    for( const auto& d : local )
      pack( s, d );
  }
template< InputStreamable StreamType, typename T, typename Allocator = std::allocator<T> >
inline void unpack( StreamType& s, std::forward_list< T, Allocator >& data )
  {
    size_t size = 0;
    unpack( s, size );
    for( size_t i = 0; i < size; ++i )
    {
      T temp;
      unpack( s, temp );
      data.push_front( temp );
    }
  }

template< OutputStreamable StreamType, typename T, typename Container = std::deque<T> >
inline void pack( StreamType& s, const std::queue< T, Container >& data )
  {
    pack( s, static_cast< size_t >( data.size() ) );
    auto local = data;
    for( size_t i = 0; i < data.size(); ++i )
    {
      pack( s, local.front() );
      local.pop();
    }
  }
template< InputStreamable StreamType, typename T, typename Container = std::deque<T> >
inline void unpack( StreamType& s, std::queue< T, Container >& data )
  {
    size_t size = 0;
    unpack( s, size );
    for( size_t i = 0; i < size; ++i )
    {
      T temp;
      unpack( s, temp );
      data.push( temp );
    }
  }

template<
  InputStreamable StreamType,
  typename T,
  typename Container = std::vector<T>,
  typename Compare = std::less<typename Container::value_type>
>
inline void pack( StreamType& s, std::priority_queue< T, Container, Compare >& data )
  {
    pack( s, static_cast< size_t >( data.size() ) );
    auto local = data;
    for( size_t i = 0; i < data.size(); ++i )
    {
      pack( s, local.top() );
      local.pop();
    }
  }

template<
  InputStreamable StreamType,
  typename T,
  typename Container = std::vector<T>,
  typename Compare = std::less<typename Container::value_type>
>
inline void unpack( StreamType& s, std::priority_queue< T, Container, Compare >& data )
  {
    size_t size = 0;
    unpack( s, size );
    for( size_t i = 0; i < size; ++i )
    {
      T temp;
      unpack( s, temp );
      data.push( temp );
    }
  }

template< OutputStreamable StreamType, typename T, typename Container = std::deque<T> >
inline void pack( StreamType& s, const std::stack< T, Container >& data )
  {
    pack( s, static_cast< size_t >( data.size() ) );
    auto local = data;
    for( size_t i = 0; i < data.size(); ++i )
    {
      pack( s, local.top() );
      local.pop();
    }
  }
/**
 * @note Takes relatively large amount of time to unpack due to the stack reversing in vector
 */
template< InputStreamable StreamType, typename T, typename Container = std::deque<T> >
inline void unpack( StreamType& s, std::stack< T, Container >& data )
  {
    size_t size = 0;
    unpack( s, size );
    std::vector< T > stack_data;
    stack_data.reserve( size );
    for( size_t i = 0; i < size; ++i )
    {
      T temp;
      unpack( s, temp );
      stack_data.push_back( temp );
    }
    for( auto it = stack_data.rbegin(); it != stack_data.rend(); ++it )
      data.push( *it );
  }

template< OutputStreamable StreamType, typename... Ts >
inline void pack( StreamType& s, const std::variant< Ts... >& data )
  {
    pack( s, static_cast< size_t >( data.index() ) );
    std::visit( [&]( auto&& actual_data ) {
      pack( s, actual_data );
    }, data );
  }
template< InputStreamable StreamType, typename... Ts >
inline void unpack( StreamType& s, std::variant< Ts... >& data )
  {
    size_t index = 0;
    unpack( s, index );
    data = __detail::__variant::runtime_variant_alternative<Ts...>( index );
    std::visit( [&]( auto&& __o )
    {
      unpack( s, __o );
    }, data );
  }

template< OutputStreamable StreamType, typename T1, typename T2 >
inline void pack( StreamType& s, const std::pair< T1, T2 >& data )
  {
    pack( s, data.first );
    pack( s, data.second );
  }
template< InputStreamable StreamType, typename T1, typename T2 >
inline void unpack( StreamType& s, std::pair< T1, T2 >& data )
  {
    unpack( s, data.first );
    unpack( s, data.second );
  }

template< OutputStreamable StreamType, typename T >
inline void pack( StreamType& s, const std::optional< T >& data )
  {
    pack( s, static_cast< uint8_t >( data.has_value() ) );
    if( data.has_value() )
      pack( s, data.value() );
  }
template< InputStreamable StreamType, typename T >
inline void unpack( StreamType& s, std::optional< T >& data )
  {
    bool has_value = false;
    unpack( s, has_value );
    if( has_value )
    {
      T temp;
      unpack( s, temp );
      data = temp;
    }
  }

template< OutputStreamable StreamType, typename... Ts >
inline void pack( StreamType& s, const std::tuple< Ts... >& data )
  {
    __detail::__tuple::visit( data, [&]( const auto& d ){
      pack( s, d );
    } );
  }
template< InputStreamable StreamType, typename... Ts >
inline void unpack( StreamType& s, std::tuple< Ts... >& data )
  {
    __detail::__tuple::visit( data, [&]( auto& d ){
      unpack( s, d );
    } );
  }

template< OutputStreamable StreamType,
          typename Rep,
          typename Period = std::ratio<1>
        >
inline void pack( StreamType& s, const std::chrono::duration< Rep, Period >& data )
  {
    pack( s, data.count() );
  }
template< InputStreamable StreamType,
          typename Rep,
          typename Period = std::ratio<1>
        >
inline void unpack( StreamType& s, std::chrono::duration< Rep, Period >& data )
  {
    Rep ticks;
    unpack( s, ticks );
    data = std::chrono::duration< Rep, Period >{ ticks };
  }

template< OutputStreamable StreamType,
          typename Clock,
          typename Duration = typename Clock::duration
        >
inline void pack( StreamType& s, const std::chrono::time_point< Clock, Duration >& data )
  {
    pack( s, data.time_since_epoch() );
  }
template< InputStreamable StreamType,
          typename Clock,
          typename Duration = typename Clock::duration
        >
inline void unpack( StreamType& s, std::chrono::time_point< Clock, Duration >& data )
  {
    Duration d;
    unpack( s, d );
    data = std::chrono::time_point< Clock, Duration >{ d };
  }

template< OutputStreamable StreamType, typename T >
inline void pack( StreamType& s, const std::complex< T >& data )
  {
    pack( s, data.real() );
    pack( s, data.imag() );
  }
template< InputStreamable StreamType, typename T >
inline void unpack( StreamType& s, std::complex< T >& data )
  {
    T re, im;
    unpack( s, re );
    unpack( s, im );
    data = std::complex< T >{ re, im };
  }

template< OutputStreamable StreamType, typename T >
inline void pack( StreamType& s, const std::atomic< T >& data )
  {
    pack( s, data.load() );
  }
template< InputStreamable StreamType, typename T >
inline void unpack( StreamType& s, std::atomic< T >& data )
  {
    T temp;
    unpack( s, temp );
    data.store( temp );
  }

template< OutputStreamable StreamType, typename T >
inline void pack( StreamType& s, const std::atomic_ref< T >& data )
  {
    pack( s, data.load() );
  }
template< InputStreamable StreamType, typename T >
inline void unpack( StreamType& s, std::atomic_ref< T >& data )
  {
    T temp;
    unpack( s, temp );
    data.store( temp );
  }

template< OutputStreamable StreamType, typename T >
inline void pack( StreamType& s, const std::reference_wrapper< T >& data )
  {
    pack( s, data.get() );
  }
template< InputStreamable StreamType, typename T >
inline void unpack( StreamType& s, std::reference_wrapper< T >& data )
  {
    unpack( s, data.get() );
  }

template< OutputStreamable StreamType, typename T >
inline void pack( StreamType& s, const std::valarray< T >& data )
  {
    pack( s, static_cast< size_t >( data.size() ) );
    for( const auto& d : data )
      pack( s, d );
  }
template< InputStreamable StreamType, typename T >
inline void unpack( StreamType& s, std::valarray< T >& data )
  {
    size_t size = 0;
    unpack( s, size );
    data.resize( size );
    for( size_t i = 0; i < size; ++i )
    {
      T temp;
      unpack( s, temp );
      data[ i ] = temp;
    }
  }

template< OutputStreamable StreamType, AnyPointerType PointerType >
inline void pack( StreamType&, PointerType )
  {
    assert( false && "Unable to pack the pointer to the OutputStreamable object due to changing memory address space" );
  }
template< InputStreamable StreamType, AnyPointerType PointerType >
inline void unpack( StreamType&, PointerType )
  {
    assert( false && "Unable to unpack the pointer from the OutputStreamable object due to changing memory address space" );
  }

template< InputStreamable StreamType, __detail::__streamable::MapType T >
inline void unpack( StreamType& s, T& data )
  {
    size_t size = 0;
    unpack( s, size );
    for( size_t i = 0; i < size; ++i )
    {
      std::pair< typename T::key_type, typename T::mapped_type > temp;
      unpack( s, temp );
      data.emplace( temp ); // Note: requires copy constructor of T::value_type
    }
  }
template< OutputStreamable StreamType, Container T >
inline void pack( StreamType& s, const T& data )
  {
    pack( s, static_cast< size_t >( data.size() ) );
    for( const auto& d : data )
      pack( s, d );
  }
template< InputStreamable StreamType, Container T >
inline void unpack( StreamType& s, T& data )
  {
    size_t size = 0;
    unpack( s, size );
    for( size_t i = 0; i < size; ++i )
    {
      typename T::value_type temp;
      unpack( s, temp );
      data.emplace( temp ); // Note: requires copy constructor of T::value_type
    }
  }
template< InputStreamable StreamType, typename T, typename Allocator = std::allocator<T> >
inline void unpack( StreamType& s, std::list< T, Allocator >& data )
  {
    size_t size = 0;
    unpack( s, size );
    for( size_t i = 0; i < size; ++i )
    {
      T temp;
      unpack( s, temp );
      data.emplace_back( temp ); // Note: requires copy constructor of T
    }
  }
template< InputStreamable StreamType, typename T, typename Allocator = std::allocator<T> >
inline void unpack( StreamType& s, std::deque< T >& data )
  requires ( !is_any_pointer_v< T > )
  {
    size_t size = 0;
    unpack( s, size );
    for( size_t i = 0; i < size; ++i )
    {
      T temp;
      unpack( s, temp );
      data.emplace_back( temp ); // Note: requires copy constructor of T
    }
  }
template< OutputStreamable StreamType, ContiguousDataContainer T >
inline void pack( StreamType& s, const T& data )
  requires ( !is_any_pointer_v< typename T::value_type > )
  {
    pack( s, static_cast< size_t >( data.size() ) );
    if constexpr( std::is_fundamental_v<typename T::value_type> )
      s.write( reinterpret_cast< const uint8_t* >( std::data(data) ), data.size() * sizeof( typename T::value_type ) );
    else
      for( const auto& d : data )
        pack( s, d );
  }
template< InputStreamable StreamType, ContiguousDataContainer T >
inline void unpack( StreamType& s, T& data )
  requires ( !is_any_pointer_v< typename T::value_type > )
  {
    size_t size = 0;
    unpack( s, size );
    __detail::__streamable::before_contiguous_unpack< T >{}( data, size );
    if constexpr( std::is_fundamental_v<typename T::value_type> )
      s.readsome( reinterpret_cast< uint8_t* >( std::data(data) ), size * sizeof( typename T::value_type ) );
    else
      for( auto& d : data )
        unpack( s, d );
  }

} // cpptools

template< cpptools::OutputStreamable StreamType, typename T >
inline StreamType& operator <<( StreamType& s, const T& data )
  {
    cpptools::pack( s, data );
    return s;
  }

template< cpptools::InputStreamable StreamType, typename T >
inline StreamType& operator >>( StreamType& s, T& data )
  {
    cpptools::unpack( s, data );
    return s;
  }
