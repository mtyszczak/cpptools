#pragma once

#include <cstddef>
#include <cassert>
#include <cstring>
#include <type_traits>
#include <initializer_list>

#include <cpptools/exception.hpp>
#include <cpptools/concepts.hpp>

namespace cpptools {

template< typename T, size_t... Args >
class array;

template< typename T, size_t Arg >
class array< T, Arg >
{
public:
  using parent             = std::decay_t< T >;
  using value_type         = parent;
  using underlying_type    = value_type;
  using array_type         = parent[ Arg ];
  using iterator           = parent*;
  using const_iterator     = const parent*;
  using reference          = parent&;
  using const_reference    = const parent&;
  using initializer_list_t = std::initializer_list< underlying_type >;

  static constexpr size_t total_size = Arg;

protected:
  array_type data = {};

public:
  constexpr array() = default;

  constexpr array( const_iterator first, const_iterator last )
  { CPPTOOLS_ASSERT( (last-first) <= Arg ); std::copy( first, last, reinterpret_cast< underlying_type* >( &data[0] ) ); }

  constexpr array( const_iterator other, size_t length )
  { CPPTOOLS_ASSERT( length <= Arg ); std::copy_n( other, length, reinterpret_cast< underlying_type* >( &data[0] ) ); }

  constexpr array( const initializer_list_t& l )
  { CPPTOOLS_ASSERT( l.size() <= Arg ); std::copy_n( std::data(l), l.size(), reinterpret_cast< underlying_type* >( &data[0] ) ); }

  constexpr size_t size()const { return Arg; }

  constexpr iterator begin() { return data; }
  constexpr iterator end()   { return data+Arg; }

  constexpr const_iterator begin()const { return data; }
  constexpr const_iterator end()const   { return data+Arg; }

  constexpr reference at( size_t index )
  {
    CPPTOOLS_ASSERT( index < Arg, "Index out of bounds" );
    return data[ index ];
  }
  constexpr const_reference at( size_t index )const
  {
    CPPTOOLS_ASSERT( index < Arg, "Index out of bounds" );
    return data[ index ];
  }

  constexpr reference operator[]( size_t index )
  {
    CPPTOOLS_ASSERT( index < Arg, "Index out of bounds" );
    return data[ index ];
  }
  constexpr const_reference operator[]( size_t index )const
  {
    CPPTOOLS_ASSERT( index < Arg, "Index out of bounds" );
    return data[ index ];
  }
};

template< typename T, size_t Arg, size_t... Args >
class array< T, Arg, Args... >
{
public:
  using parent             = array< std::decay_t<T>, Args... >;
  using value_type         = parent::array_type;
  using underlying_type    = parent::underlying_type;
  using array_type         = parent[ Arg ];
  using iterator           = parent*;
  using const_iterator     = const parent*;
  using reference          = parent&;
  using const_reference    = const parent&;
  using initializer_list_t = std::initializer_list< typename parent::initializer_list_t >;

  static constexpr size_t total_size = Arg * parent::total_size;

protected:
  array_type data = {};

public:
  constexpr array() = default;

  constexpr array( const_iterator first, const_iterator last )
  { CPPTOOLS_ASSERT( (last-first) <= Arg ); std::copy( first, last, reinterpret_cast< underlying_type* >( &data[0] ) ); }

  constexpr array( const_iterator other, size_t length )
  { CPPTOOLS_ASSERT( length <= Arg ); std::copy_n( other, length, reinterpret_cast< underlying_type* >( &data[0] ) ); }

  constexpr array( const initializer_list_t& ls )
  {
    CPPTOOLS_ASSERT( ls.size() <= Arg );
    for( size_t i = 0; const auto& l : ls )
      data[i++] = parent{ l };
  }

  constexpr size_t size()const { return Arg; }

  constexpr iterator begin() { return data; }
  constexpr iterator end()   { return data+Arg; }

  constexpr const_iterator begin()const { return data; }
  constexpr const_iterator end()const   { return data+Arg; }

  constexpr reference at( size_t index )
  {
    CPPTOOLS_ASSERT( index < Arg, "Index out of bounds" );
    return data[ index ];
  }
  constexpr const_reference at( size_t index )const
  {
    CPPTOOLS_ASSERT( index < Arg, "Index out of bounds" );
    return data[ index ];
  }

  constexpr reference operator[]( size_t index )
  {
    CPPTOOLS_ASSERT( index < Arg, "Index out of bounds" );
    return data[ index ];
  }
  constexpr const_reference operator[]( size_t index )const
  {
    CPPTOOLS_ASSERT( index < Arg, "Index out of bounds" );
    return data[ index ];
  }
};

} // cpptools

template< typename T, size_t... N >
inline auto operator ==( const cpptools::array< T, N... >& a, const cpptools::array< T, N... >& b )
  { return memcmp( &a[0], &b[0], sizeof( T ) * cpptools::array< T, N... >::total_size ) == 0; }

template< typename T, size_t... N >
inline auto operator <=>( const cpptools::array< T, N... >& a, const cpptools::array< T, N... >& b )
  { return memcmp( &a[0], &b[0], sizeof( T ) * cpptools::array< T, N... >::total_size ); }

namespace cpptools {

  template< OutputStreamable StreamType, typename T, size_t... N >
  inline void pack( StreamType& s, const array< T, N... >& data )
    requires ( !is_any_pointer_v< T > )
  {
  if( std::is_fundamental_v<T> )
    s.write( reinterpret_cast< const uint8_t* >( &data[0] ), array< T, N... >::total_size * sizeof( T ) );
  else
    for( size_t i = 0; i < array< T, N... >::total_size; ++i )
      pack( s, *( (&data[0]) + i ) );
  }

  template< InputStreamable StreamType, typename T, size_t... N >
  inline void unpack( StreamType& s, array< T, N... >& data )
    requires ( !is_any_pointer_v< T > )
  {
  if( std::is_fundamental_v<T> )
    s.readsome( reinterpret_cast< uint8_t* >( &data[0] ), array< T, N... >::total_size * sizeof( T ) );
  else
    for( size_t i = 0; i < array< T, N... >::total_size; ++i )
      unpack( s, *( (&data[0]) + i ) );
  }

} // cpptools
