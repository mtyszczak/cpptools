#pragma once

#include <cpptools/streamable.hpp>
#include <cpptools/array.hpp>
#include <cpptools/math.hpp>
#include <cpptools/exception.hpp>

#include <initializer_list>
#include <type_traits>
#include <cstring>

namespace cpptools {

  namespace __detail { namespace __hash {
    template< typename T >
    concept HashBaseType = std::is_object_v< T >;
  } } // __detail::__hash

  template< __detail::__hash::HashBaseType T, size_t N >
  class hash : public array< T, N >
  {
  public:
    explicit constexpr hash() = default;

    explicit constexpr hash( const T* other, size_t size )         : array<T,N>( other, size ) {}
    explicit constexpr hash( const T* first, const T* last )       : array<T,N>( first, last ) {}
    explicit constexpr hash( const std::initializer_list< T >& l ) : array<T,N>( l ) {}

    explicit hash( const std::string& str )
    { __detail::__math::from_hex( str, reinterpret_cast< __detail::__math::hex_unit_t* >( array<T,N>::data ), ( sizeof(T)/sizeof(__detail::__math::hex_unit_t) ) * N ); }

    inline operator std::string()const
    { return __detail::__math::to_hex( reinterpret_cast< const __detail::__math::hex_unit_t* >( array<T,N>::data ), ( sizeof(T)/sizeof(__detail::__math::hex_unit_t) ) * N ); }

    /// std::hash<> compatible function
    inline operator size_t()const
    {
      size_t _out = 0;
      for( size_t i = 0; i < N; ++i )
        _out += ( std::hash< T >{}( array<T,N>::data[i] ) + 31 ) * (i+1);
      return _out;
    }
  };

  template< __detail::__hash::HashBaseType T >
  class hash< T, 1 > : public array< T, 1 >
  {
  public:
    explicit constexpr hash() = default;

    explicit constexpr hash( const T& __o ) // enable explicit hash type constructor
    { array<T,1>::data[0] = __o; }

    explicit constexpr hash( const T* other, size_t size )         : array<T,1>( other, size ) {}
    explicit constexpr hash( const T* first, const T* last )       : array<T,1>( first, last ) {}
    explicit constexpr hash( const std::initializer_list< T >& l ) : array<T,1>( l ) {}

    explicit hash( const std::string& str )
      : array<T,1>()
    { __detail::__math::from_hex( str, reinterpret_cast< __detail::__math::hex_unit_t* >( array<T,1>::data ), sizeof(T)/sizeof(__detail::__math::hex_unit_t) ); }

    operator std::string()const
    { return __detail::__math::to_hex( reinterpret_cast< const __detail::__math::hex_unit_t* >( array<T,1>::data ), sizeof(T)/sizeof(__detail::__math::hex_unit_t) ); }

    /// std::hash<> compatible function
    operator size_t()const
    { return std::hash< T >{}( array<T,1>::data[ 0 ] ) + 31; }

    inline T* operator->()noexcept
    { return array<T,1>::data; }

    inline T& operator*()
    { return array<T,1>::data[0]; }
  };

  template< __detail::__hash::HashBaseType T >
  class hash< T, 0 > : public vector< T, 1 >
  {
  public:
    explicit constexpr hash() = default;

    explicit constexpr hash( const T* other, size_t size )         : vector<T,1>( other, size ) {}
    explicit constexpr hash( const T* first, const T* last )       : vector<T,1>( first, last ) {}
    explicit constexpr hash( const std::initializer_list< T >& l ) : vector<T,1>( l ) {}

    explicit hash( const std::string& str )
    {
      size_t i = ( sizeof(T)/sizeof(__detail::__math::hex_unit_t) ) * (str.size()/2);
      cpptools::vector<T,1>::resize(i);
      __detail::__math::from_hex( str, reinterpret_cast< __detail::__math::hex_unit_t* >( cpptools::vector<T,1>::data() ), i );
    }

    inline operator std::string()const
    { return __detail::__math::to_hex( reinterpret_cast< const __detail::__math::hex_unit_t* >( cpptools::vector<T,1>::data() ), ( sizeof(T)/sizeof(__detail::__math::hex_unit_t) ) * cpptools::vector<T,1>::size() ); }

    /// std::hash<> compatible function
    inline operator size_t()const
    {
      size_t _out = 0;
      for( size_t i = 0; i < cpptools::vector<T,1>::size(); ++i )
        _out += ( std::hash< T >{}( cpptools::vector<T,1>::at(i) ) + 31 ) * (i+1);
      return _out;
    }
  };

} // cpptools

namespace std
{
  template< typename T, size_t N >
  struct hash< cpptools::hash< T, N > >
  {
    size_t operator()( const cpptools::hash< T, N >& __h ) const noexcept
    { return __h.operator size_t(); }
  };
} // std

template< typename T, size_t N >
inline auto operator ==( const cpptools::hash< T, N >& a, const cpptools::hash< T, N >& b )
  { return memcmp( &a[0], &b[0], sizeof( T ) * N ) == 0; }

template< typename T, size_t N >
inline auto operator <=>( const cpptools::hash< T, N >& a, const cpptools::hash< T, N >& b )
  { return memcmp( &a[0], &b[0], sizeof( T ) * N ); }

namespace cpptools {

  template< OutputStreamable StreamType, typename T, size_t N >
  inline void pack( StreamType& s, const hash< T, N >& data )
    requires ( !is_any_pointer_v< T > )
  {
  if( std::is_fundamental_v<T> )
    s.write( reinterpret_cast< const uint8_t* >( &data[0] ), N * sizeof( T ) );
  else
    for( size_t i = 0; i < N; ++i )
      pack( s, *( (&data[0]) + i ) );
  }

  template< InputStreamable StreamType, typename T, size_t N >
  inline void unpack( StreamType& s, hash< T, N >& data )
    requires ( !is_any_pointer_v< T > )
  {
  if( std::is_fundamental_v<T> )
    s.readsome( reinterpret_cast< uint8_t* >( &data[0] ), N * sizeof( T ) );
  else
    for( size_t i = 0; i < N; ++i )
      unpack( s, *( (&data[0]) + i ) );
  }

} // cpptools
