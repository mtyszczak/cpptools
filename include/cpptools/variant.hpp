#pragma once

#include <cstring>
#include <variant>
#include <cassert>

namespace cpptools {

namespace __detail { namespace __variant {

  template < typename... Ts >
  [[nodiscard]] std::variant< Ts... > runtime_variant_alternative( size_t index )
  {
    assert( index < sizeof...( Ts ) );
    static std::variant< Ts... > variants[] = { Ts{}... };
    return variants[ index ];
  }

} } // __detail::__variant

} // cpptools
