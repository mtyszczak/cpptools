#pragma once

#include <cstdint>
#include <cstddef>

#include <string>

namespace cpptools {

  namespace __detail { namespace __math {

  typedef uint8_t hex_unit_t;

  /// Converts decimal values 0-15 to the corresponding hex characters
  char       to_hex( hex_unit_t _u );
  hex_unit_t from_hex( char _u );

  std::string to_hex( const hex_unit_t* in_buff, size_t in_size );
  /// Converts hex_str into the out_buff
  size_t      from_hex( const std::string& hex_str, hex_unit_t* out_buff, size_t out_size );

  } } // __detail::__math

} // cpptools
