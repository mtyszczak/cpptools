#pragma once

#include <memory>
#include <map>
#include <unordered_map>
#include <type_traits>

namespace cpptools {

class ios_base;
class istream;
class ostream;

// XXX: Note: this won't work for all templates, such as std::array
template <typename T, template <typename...> typename Template>
struct is_specialization : std::false_type {};

template <template <typename...> class Template, typename... Args>
struct is_specialization<Template<Args...>, Template> : std::true_type {};

template <typename T, template <typename...> typename Template>
constexpr bool is_specialization_v = is_specialization< T, Template >::value;

template< typename T >
concept SmartPointerType = is_specialization_v< T, std::unique_ptr > || is_specialization_v< T, std::shared_ptr > || is_specialization_v< T, std::weak_ptr >;

template< typename T >
constexpr bool is_smart_pointer_v = SmartPointerType< T >;

template< typename T >
concept AnyPointerType = std::is_pointer_v< T > || SmartPointerType< T >;

template< typename T >
constexpr bool is_any_pointer_v = AnyPointerType< T >;

template< typename... Ts >
struct is_any_pointer_in;

template< typename T, typename... Ts >
struct is_any_pointer_in< T, Ts... >
{
  static constexpr bool value = is_any_pointer_v< T > || is_any_pointer_in< Ts... >::value;
};

template<>
struct is_any_pointer_in<>
{
  static constexpr bool value = false;
};

template< typename... Ts >
constexpr bool is_any_pointer_in_v = is_any_pointer_in< Ts... >::value;

template< typename T >
concept StreamableBase = std::is_base_of_v< ios_base, T >;

template< typename T >
constexpr bool is_streamable_v = StreamableBase< T >;

template< typename T >
concept OutputStreamable = std::is_base_of_v< ostream, T >;

template< typename T >
constexpr bool is_output_streamable_v = OutputStreamable< T >;

template< typename T >
concept InputStreamable = std::is_base_of_v< istream, T >;

template< typename T >
constexpr bool is_input_streamable_v = InputStreamable< T >;

/// Container is everything that has size and is iterable
template< typename T >
concept Container = requires( T t ) {
                      { std::size(t) } -> std::convertible_to<size_t>;
                      { *std::begin(t) } -> std::convertible_to< typename T::value_type >;
                    } && std::indirectly_readable< typename T::iterator >;

template< typename T >
constexpr bool is_container_v = Container< T >;

/// Adaptor container is everything that has size, but is not iterable
template< typename T >
concept AdaptorContainer = requires( T t ) {
                      { std::size(t) } -> std::convertible_to<size_t>;
                      { t.pop() };
                    };

template< typename T >
constexpr bool is_adaptor_container_v = AdaptorContainer< T >;

/// Contiguous data container is everything that has size and is iterable, but underlying data is being held in a contiguous block od data in memory
template< typename T >
concept ContiguousDataContainer = requires( T t ) { { *std::data(t) } -> std::convertible_to< typename T::value_type >; } && Container< T >;

template< typename T >
constexpr bool is_contiguous_data_container_v = ContiguousDataContainer< T >;

} // cpptools
