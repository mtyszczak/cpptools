#pragma once

#include <cpptools/preprocessor.hpp>
#include <cpptools/streamable.hpp>

#include <iostream>
#include <iomanip>
#include <cstring>

namespace cpptools {

  namespace __detail { namespace __logger {

  struct end {};

  struct fnl {};

  struct null_indicator {};

  static constexpr auto null = null_indicator{};

  } } // __detail::__logger

  static constexpr auto endl = __detail::__logger::end{};
  /// Fixed new line
  static constexpr auto fnl  = __detail::__logger::fnl{};

  enum class logging_level: uint8_t
  {
    debug   = 0,
    info    = 1,
    warning = 2,
    error   = 3
  };

#ifndef CPPTOOLS_MESSAGE_FORMATTING_COLORS
# define CPPTOOLS_MESSAGE_FORMATTING_RESET  "\033[0m"
# define CPPTOOLS_MESSAGE_FORMATTING_YELLOW "\033[33m"
# define CPPTOOLS_MESSAGE_FORMATTING_RED    "\033[31m"
# define CPPTOOLS_MESSAGE_FORMATTING_BLUE   "\033[34m"

# define CPPTOOLS_MESSAGE_FORMATTING_COLORS
#endif

  class logger : public ostream
  {
  private:
    std::string name;
  public:
    logger( const std::string& name = "unknown" );

    const std::string& get_name()const;

    virtual void write( const uint8_t*, size_t ) override;
  };

  class null_logger final : public logger
  {
  public:
    null_logger( const std::string& name = "unknown" );
  };

  class standard_logger final : public logger
  {
  public:
    standard_logger( const std::string& name = "unknown" );

    virtual void write( const uint8_t* data, size_t length ) override;
  };

  static inline const standard_logger out_log{ "out_log" };

  static inline const null_logger stdnull{ "stdnull" };

#define CPPTOOLS_MESSAGE_WITH_FILE_NUMBER_FUNC( chain, filename, number, func, message )                      \
  (chain) << std::left << std::setw(20) << ( std::string{ strrchr( (filename), '/')+1 }.substr(0,15) + ":" + std::to_string( (number) ).substr(0,5) ) \
        << ' ' << std::right << std::setw(15) << std::string{ (func) }.substr(0,15) << "  ] " << std::left << message

#define CPPTOOLS_MESSAGE_IMPL( logger_type, level, filename, number, func, message ) \
  CPPTOOLS_MESSAGE_WITH_FILE_NUMBER_FUNC( \
    ((logger_type) << std::left << std::setw(11) << ((logger_type).get_name().substr(0,10) + ':') << ' ' << (level)), \
    __FILE__, __LINE__, __func__, message )

#define CPPTOOLS_MESSAGE( logger_type, level, message ) CPPTOOLS_MESSAGE_IMPL( logger_type, level, __FILE__, __LINE__, __func__, message )

#define CPPTOOLS_MESSAGE_FORWARD_LEVEL( logger_type, level, message ) \
  CPPTOOLS_MESSAGE( logger_type, level, message )

#define CPPTOOLS_DLOG_WITH_LOGGER( logger_type, message ) CPPTOOLS_MESSAGE_FORWARD_LEVEL( logger_type, cpptools::logging_level::debug, message )
#define CPPTOOLS_ILOG_WITH_LOGGER( logger_type, message ) CPPTOOLS_MESSAGE_FORWARD_LEVEL( logger_type, cpptools::logging_level::info, message )
#define CPPTOOLS_WLOG_WITH_LOGGER( logger_type, message ) CPPTOOLS_MESSAGE_FORWARD_LEVEL( logger_type, cpptools::logging_level::warning, message )
#define CPPTOOLS_ELOG_WITH_LOGGER( logger_type, message ) CPPTOOLS_MESSAGE_FORWARD_LEVEL( logger_type, cpptools::logging_level::error, message )

#define cpptools_dlog CPPTOOLS_DLOG_WITH_LOGGER( cpptools::out_log, cpptools::__detail::__logger::null )
#define cpptools_ilog CPPTOOLS_ILOG_WITH_LOGGER( cpptools::out_log, cpptools::__detail::__logger::null )
#define cpptools_wlog CPPTOOLS_WLOG_WITH_LOGGER( cpptools::out_log, cpptools::__detail::__logger::null )
#define cpptools_elog CPPTOOLS_ELOG_WITH_LOGGER( cpptools::out_log, cpptools::__detail::__logger::null )

} // cpptools

const cpptools::null_logger& operator<<( const cpptools::null_logger& logger, std::ostream& (*)(std::ostream&));

const cpptools::null_logger& operator<<( const cpptools::null_logger& logger, std::ios& (*)(std::ios&));

const cpptools::null_logger& operator<<( const cpptools::null_logger& logger, std::ios_base& (*)(std::ios_base&));

template< typename T >
  constexpr const cpptools::null_logger& operator<<( const cpptools::null_logger& logger, const T& )
{ return logger; }


const cpptools::standard_logger& operator<<( const cpptools::standard_logger& logger, cpptools::logging_level level );

const cpptools::standard_logger& operator<<( const cpptools::standard_logger& logger, cpptools::__detail::__logger::null_indicator );

void operator<<( const cpptools::standard_logger& logger, cpptools::__detail::__logger::end );

const cpptools::standard_logger& operator<<( const cpptools::standard_logger& logger, cpptools::__detail::__logger::fnl );

const cpptools::standard_logger& operator<<( const cpptools::standard_logger& logger, std::ostream& (*__pf)(std::ostream&));

const cpptools::standard_logger& operator<<( const cpptools::standard_logger& logger, std::ios& (*__pf)(std::ios&));

const cpptools::standard_logger& operator<<( const cpptools::standard_logger& logger, std::ios_base& (*__pf)(std::ios_base&));

template< typename T >
  inline const cpptools::standard_logger& operator<<( const cpptools::standard_logger& logger, const T& t )
{
  std::cout << t; // TODO: Serialization
  return logger;
}
