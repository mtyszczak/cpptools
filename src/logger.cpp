#include <iostream>
#include <cstring>

#include <cpptools/logger.hpp>

namespace cpptools {

  namespace __detail { namespace __logger {
    static const char* logger_new_line_prefix = "                                                    ";
  } } // __detail::__logger

  logger::logger( const std::string& name )
    : name( name )
  {}

  const std::string& logger::get_name()const
  { return name; }

  void logger::write( const uint8_t*, size_t )
  {}


  null_logger::null_logger( const std::string& name )
    : logger( name )
  {}


  standard_logger::standard_logger( const std::string& name )
    : logger( name )
  {
#ifdef _WIN32
    SetConsoleMode(GetStdHandle(STD_OUTPUT_HANDLE), ENABLE_VIRTUAL_TERMINAL_PROCESSING);
#endif
  }

  void standard_logger::write( const uint8_t* data, size_t length )
  { *this << ( std::string{ reinterpret_cast< const char* >( data ), length } ); }

} // cpptools

const cpptools::null_logger& operator<<( const cpptools::null_logger& logger, std::ostream& (*)(std::ostream&))
{ return logger; }

const cpptools::null_logger& operator<<( const cpptools::null_logger& logger, std::ios& (*)(std::ios&))
{ return logger; }

const cpptools::null_logger& operator<<( const cpptools::null_logger& logger, std::ios_base& (*)(std::ios_base&))
{ return logger; }


const cpptools::standard_logger& operator<<( const cpptools::standard_logger& logger, cpptools::logging_level level )
{
  switch( level )
  {
    case cpptools::logging_level::warning:
      return logger <<( CPPTOOLS_MESSAGE_FORMATTING_YELLOW );
    case cpptools::logging_level::error:
      return logger <<( CPPTOOLS_MESSAGE_FORMATTING_RED );
    case cpptools::logging_level::info:
      return logger <<( CPPTOOLS_MESSAGE_FORMATTING_BLUE );
    default: case cpptools::logging_level::debug:
      return logger <<( CPPTOOLS_MESSAGE_FORMATTING_RESET );
  }
}

const cpptools::standard_logger& operator<<( const cpptools::standard_logger& logger, cpptools::__detail::__logger::null_indicator )
{ return logger; }

void operator<<( const cpptools::standard_logger& logger, cpptools::__detail::__logger::end )
{ logger <<( CPPTOOLS_MESSAGE_FORMATTING_RESET ) <<( std::endl ); }

const cpptools::standard_logger& operator<<( const cpptools::standard_logger& logger, cpptools::__detail::__logger::fnl )
{ logger <<( '\n' ) <<( cpptools::__detail::__logger::logger_new_line_prefix ); return logger; }

const cpptools::standard_logger& operator<<( const cpptools::standard_logger& logger, std::ostream& (*__pf)(std::ostream&))
{ std::cout << __pf; return logger; }

const cpptools::standard_logger& operator<<( const cpptools::standard_logger& logger, std::ios& (*__pf)(std::ios&))
{ std::cout << __pf; return logger; }

const cpptools::standard_logger& operator<<( const cpptools::standard_logger& logger, std::ios_base& (*__pf)(std::ios_base&))
{ std::cout << __pf; return logger; }
