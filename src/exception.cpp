#include <cpptools/exception.hpp>

#include <utility>

namespace cpptools {

  exception::exception( const exception_trip_node& _next )
  {
    record( _next );
  }
  exception::exception( const trip_type& __rhs )
    : trip( __rhs )
  {}
  exception::exception( const trip_type& __rhs, const exception_trip_node& _next )
    : trip( __rhs )
  {
    record( _next );
  }

  void exception::record( const exception_trip_node& node )
  { trip.push_back( node ); }

  const exception::trip_type& exception::get_trip()const
  { return trip; }

  void exception::display_trip()const
  {
    for( unsigned depth = 0; const auto& node : trip )
    {
      CPPTOOLS_MESSAGE_IMPL( error_log, cpptools::logging_level::error, node.file.c_str(), node.line, node.function,
        depth++ << ": Exception [" << node.type << "] caught: " << node.message << cpptools::endl
      );
    }
  }

  const char* exception::what()const
  {
    if( trip.begin() != trip.end() )
      return ( trip.end() - 1 )->message.c_str();
    return "Could not display exception message. No trip recorded";
  }

} // cpptools
