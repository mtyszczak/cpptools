#include <cpptools/streamable.hpp>

#include <cassert>

#include <SHA256.h>

namespace cpptools {

const ios_base::raw_data_type& ios_base::get_data()const
  { return data; }

size_t ios_base::size()const
  { return data.size(); }

void istream::write( [[maybe_unused]] const uint8_t* data, [[maybe_unused]] size_t length )
  { assert( false && "You cannot write to input-only stream class" ); }

void ostream::readsome( [[maybe_unused]] uint8_t* data, [[maybe_unused]] size_t length )
  { assert( false && "You cannot read from output-only stream class" ); }

namespace null {

  size_t counter::get_total()const
  { return total; }

  void read_counter::readsome( [[maybe_unused]] uint8_t* data, size_t length )
  { total += length; }

  void write_counter::write( [[maybe_unused]] const uint8_t* data, size_t length )
  { total += length; }

  void read_write_counter::write( const uint8_t* data, size_t length )
  { write_counter::write( data, length ); }
  void read_write_counter::readsome( uint8_t* data, size_t length )
  { read_counter::readsome( data, length ); }

} // null

namespace SHA256 {

  void encoder::write( const uint8_t* data, size_t length )
  { /* TODO: maybe assert length >= 0 */ SHA256::update( data, length ); }
  uint8_t* encoder::digest()
  {
    uint8_t* digest_data = SHA256::digest();
    memcpy( &data, digest_data, sizeof( data ) );
    delete[] digest_data;
    return &data[0];
  }

} // SHA256

namespace raw {

  void ostream::write( const uint8_t* data, size_t length )
  { this->data.insert( this->data.end(), data, data + length ); }

  void istream::readsome( uint8_t* data, size_t length )
  {
    assert( current_offset + length <= this->data.size() && "end of buffer" );
    memcpy( data, this->data.data() + current_offset, length );
    current_offset += length;
    if( current_offset == this->data.size() ) // Clear the buffer if empty
    {
      this->data.clear();
      current_offset = 0;
    }
  }

  void iostream::write( const uint8_t* data, size_t length )
  { ostream::write( data, length ); }
  void iostream::readsome( uint8_t* data, size_t length )
  { istream::readsome( data, length ); }

} // raw

} // cpptools
