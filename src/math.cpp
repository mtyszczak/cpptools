
#include <cpptools/math.hpp>

#include <cstdint> // For uint8_t
#include <cstddef> // For size_t
#include <cassert>
#include <string>

#include <cpptools/exception.hpp>

namespace cpptools {

  namespace __detail { namespace __math {

  static constexpr const char* HEX_LOOKUP_TABLE = "0123456789abcdef";

  char to_hex( hex_unit_t _u )
  {
    if( _u > 15 ) return '0';
    return HEX_LOOKUP_TABLE[ _u ];
  }

  hex_unit_t from_hex( char _u )
  {
    if( _u >= '0' && _u <= '9' )
      return _u - '0';
    if( _u >= 'a' && _u <= 'f' )
      return _u - 'a' + 10;
    if( _u >= 'A' && _u <= 'F' )
      return _u - 'A' + 10;
    return 0;
  }

  std::string to_hex( const hex_unit_t* in_buff, size_t in_size )
  {
    std::string _out;
    _out.reserve( in_size );
    for( size_t i = 0; i < in_size; ++i )
    // for( size_t i = in_size; i > 0; --i )
      (_out += HEX_LOOKUP_TABLE[(in_buff[i/*-1*/]>>4)]) += HEX_LOOKUP_TABLE[(in_buff[i/*-1*/] &0x0f)];
    return _out;
  }

  size_t      from_hex( const hex_unit_t* in_buff, size_t in_size )
  {
    CPPTOOLS_ASSERT( in_size <= 16, "Max compatible size is strlen of sizeof( size_t ) in hex which is 16" );
    size_t _out = 0;
    const hex_unit_t* buff_end = in_buff + in_size;
    while( in_buff != buff_end )
    {
      _out += from_hex( *in_buff ) << 4;
      ++in_buff;
    }
    return _out;
  }

  size_t from_hex( const std::string& hex_str, hex_unit_t* out_buff, size_t out_size )
  {
    auto u_itr = hex_str.begin();
    hex_unit_t* out_begin = out_buff;
    hex_unit_t* out_end = out_begin + out_size;
    while( u_itr != hex_str.end() && out_end != out_buff ) {
      *out_buff = from_hex( *u_itr ) << 4;
      ++u_itr;
      if( u_itr != hex_str.end() )  {
          *out_buff |= from_hex( *u_itr );
          ++u_itr;
      }
      ++out_buff;
    }
    return out_buff - out_begin;
  }

  } } // __detail::__math

} // cpptools
